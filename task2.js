//http://acmu.ru/index.asp?main=task&id_task=2

function sum(n) {
    if (n === 1) return 1;
    return n + sum(n - 1);
}

function sum1(n) {
    let sum = 0;
    for (let i = 1; i <= n; i++) {
        sum += i;
    }
    return sum;
}

console.log(sum(5));
console.log(sum1(5));