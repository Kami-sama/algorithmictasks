//http://acmu.ru/?main=task&id_task=5

let a = [4, 16, 19, 31, 2];

function f(array) {
    let result = groupBy(array, e => e % 2);
    console.log(result[0]);
    console.log(result[1]);
    console.log(function () {
        if (result[0].length >= result[1].length)
            return "Yes";
        return "No";
    }());
}

var groupBy = function (array, func) {
    return array.reduce(function (result, e) {
        (result[func(e)] = result[func(e)] || []).push(e);
        return result;
    }, {});
};

f(a);