function f() {
    let a = getRandom();
    let c = getRandom();
    while (c === a) {
        c = getRandom();
    }

    let b = getRandom();

    let d = a * 100 + b * 10 + c;

    let reverseD = c * 100 + b * 10 + a;
    let diff = Math.abs(d - reverseD);

    console.log("Random digit:" + d);
    console.log("Reverse " + reverseD);
    console.log("Diff: " + diff);
}

function getRandom() {
    return Math.floor(Math.random() * 9) + 1;
}


console.log(f())
