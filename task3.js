function pow5two(n) {
    if (n > 40000)
        return "Number cannot be more that 40 000"

    if (n % 10 === 5) {
        let a = n - 5;
        return a * (a + 10) + 25;
    }

    return "Don't ends with 5";
}

console.log(pow5two(12));
console.log(pow5two(40001));
console.log(pow5two(125));
console.log(pow5two(5));
console.log(pow5two(25));
console.log(pow5two(525));
console.log(pow5two(6545));

