console.log(r([3, 2, 1]));
console.log(r([1, 2, 3]));
console.log(r([1, 3, 2]));
console.log(r([2, 3, 1]));
console.log(r([1, 2, 3, 4]));
console.log(r([1, 2, 4, 3]));
console.log(r([2, 1, 4, 3]));
console.log(r([2, 4, 3, 1]));
console.log(r([4, 3, 2, 1]));

function r(a) {
    let last = a.length - 1;
    let beforeLast = last - 1;

    let cElement = 0;
    if (a.length - beforeLast > 0) {
        cElement = beforeLast - 1;
    }

    if (a[last] > a[beforeLast]) {
        swap(a, last, beforeLast);
    } else {
        let min = last;
        let max = beforeLast;

        if (a[cElement] < a[max] && a[cElement] > a[min]) {
            swap(a, cElement, max);
            swap(a, max, min);
        } else if ((a[cElement] < a[min])) {
            swap(a, cElement, min);
            swap(a, max, min);
        } else if (a[cElement] > a[max]) {
            swap(a, cElement, min);
        }


    }

    return a;
}

function swap(array, i1, i2) {
    let temp = array[i1];
    array[i1] = array[i2];
    array[i2] = temp;
}