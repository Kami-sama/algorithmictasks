let pattern = /[A-I][1-8]-[A-I][1-8]/;


function f(s) {
    if (!s.match(pattern)) return "ERROR";

    let x = Math.pow(s.charCodeAt(3) - s.charCodeAt(0), 2);
    let y = Math.pow(s.charAt(4) - s.charAt(1), 2);

    return x + y === 5 ? "Yes" : "No";

}

console.log(f("qwe"));
console.log(f("A7-B8"));
console.log(f("G5-G1"));
console.log(f("A7-D3"));
console.log(f("C7-D5"));
