function f(a) {
    return a.reduce(function (max, e) {
        if (e.length > max.length)
            return e;

        if (e.length === max.length) {
            for (let i = 0; i < e.length; i++) {
               if(max[i] < e[i])
                   return e;
            }
        }

        return max;

    }, "");
}


console.log(f(["5", "7", "3"]));
console.log(f(["987531", "234", "86364"]));
console.log(f(["189285", "283", "4958439238923098349024"]));
console.log(f(["189285", "283", "4958439238923098349024", "49584392389230983490"]));
console.log(f(["189285", "283", "4958439238923098349024", "4958439238923098349025", "4968439238923098349024"]));